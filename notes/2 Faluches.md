---
tags: [cooking]
title: 2 Faluches
created: '2020-10-04T16:35:03.951Z'
modified: '2020-10-04T16:41:31.392Z'
---

# 2 Faluches

* 500gr farine blanche
* 20gr levure fraiche
* 7gr sel
* 15gr beurre mou
* 275ml eau

--- 

* Bien pétrir le tout et laisser doubler de volume
* former 2 boules
* les poser sur une plaque de four et les aplatir
* laisser pousser 15min et les aplatir à nouveau
* saupoudrer les falushes de farine et laisser à nouveau pousser +/- 20 minutes
* enfourner à four préchauffé à 150°C pendant 20 minutes

