---
tags: [cooking]
title: Soupe
created: '2020-10-04T15:52:30.845Z'
modified: '2020-10-04T16:41:37.119Z'
---

# Soupe

* éplucher les légumes a mettre, et mettre en petit morceaux / cubes
* huile d'olive dans une casserole (grande!)
* mettre oignons et attendre que ça crépite (faire revenir)
* mettre les morceaux de légumes + un cube de bouillon émiété
* mettre de l'eau (1L/1.5L) et fermer la casserole jusqu'à ce que ça bout
* quand ça bout, réduire le feu, attendre 30 minutes
* une fois que ce temps est passé, vérifier que les légumes soit bien cuit en perforant un cube de chaque légume, ça doit être mou / tendre
* mixer le tout avec ce que j'ai reçu à noël (mixe-soupe)
* ajouter sel + poivre
* consommer / mettre en "pot" pour plus tard
* mettre au frigo une fois froid

## attention

_Si la soupe est au frais plusieur jours, faire bouillir celle ci chaque jour, sinon ça devient sûre!_
