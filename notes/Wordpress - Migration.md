---
tags: [DevOps]
title: Wordpress - Migration
created: '2020-10-04T14:05:45.427Z'
modified: '2020-10-04T19:18:47.764Z'
---

# Wordpress - Migration

* save SQL to its own folder for later use
* save PUBLIC_HTML data to its own folder for later use
* make `volumes` subfolders for docker, `db` (database), `wordpress` (for wp root folder)

* create docker compose with the following, adapt if needed:

```yaml
version: '3.1'

services:
  wordpress:
    image: wordpress
    restart: unless-stopped
    ports:
      - 39080:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: wpusername
      WORDPRESS_DB_PASSWORD: wppassword
      WORDPRESS_DB_NAME: wpdbname
    volumes:
      - ./volumes/wordpress:/var/www/html

  db:
    image: mysql:5.7
    restart: unless-stopped
    environment:
      MYSQL_DATABASE: wpdbname
      MYSQL_USER: sqluser
      MYSQL_PASSWORD: sqlpassword
      MYSQL_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - ./volumes/db:/var/lib/mysql
```

* start the database container first

```bash
docker-compose up -d db
```

* import the SQL backup into the database. Replace `[foldername_wordpress_1]` with the actual value instead of `foldername`, `[sqluser]` with the actual sql user set in the dockerfile, `[wpdbname]` with the actual database name set in the dockerfile.

```bash
# in this example the sql file is "backup.sql" in ./sql/
docker exec -i [foldername]_wordpress_1 mysql -u [sqluser] -p[sqlpassword] [wpdbname] < ./sql/backup.sql
```

* adapt `wp-config.php` to get the new credentials for database
* start the wordpress CLI to proceed to domain change

```bash
docker run -it --rm -v "$(pwd)/volumes/wordpress:/wp:rw" --network [foldername]_default wordpress:cli bash
# once logged into the container
cd /wp
wp search-replace 'yourolddomain.tld' 'localhost:39080'
wp search-replace 'https://localhost' 'http://localhost'
```

* start the wordpress docker container
```bash
docker-compose up -d wordpress
```

* Head to the website (`localhost:39080`) and attempt to login. If everything works, perfect! 

## Things to check after install

* `wp-content` sould be chmod 777 (+rwx):
```bash
chmod 777 -R $(PWD)/volumes/wordpress/wp-content
```
* make sure to update in `Settings > Media` the path for uploaded files, it should be the following:
```bash
/var/www/html/wp-content/uploads
```
* add the following to the `wp-config.php`:
```php
define('WP_MEMORY_LIMIT', '64M');

if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
   $_SERVER['HTTPS']='on';
```
