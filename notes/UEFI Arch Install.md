---
favorited: true
tags: [arch, Linux]
title: UEFI Arch Install
created: '2019-08-15T18:35:37.252Z'
modified: '2020-10-30T08:20:32.363Z'
---

# UEFI Arch Install

Load BE keyboard

> loadkeys be-latin1

Set auto-timezone stuff

> timedatectl set-ntp true

List disks, spot the one you need

> fdisk -l

start partitioning

> cfdisk /dev/xxx

set up UEFI partition by +512M, primary, type `EFI (FAT-12/16/32)` on page 2 of types
set up SWAP partition by +32G, primary, type `Linux Swap`
set up ROOT partition by whatever is left, primary, type `Linux`
set Write, confirm, quit cfdisk

>mkfs.ext4 /dev/xxx3 (Root)

>mkswap /dev/xxx2 (swap)

>swapon /dev/xxx2 (swap)

>mkfs.fat -F32 /dev/xxx1 (EFI)

Mount the ROOT folder to the /mnt folder

>mount /dev/xxx3 /mnt

Create EFI folder

>mkdir -p /mnt/boot

Mount disk of EFI to the created folder

>mount /dev/xxx1 /mnt/boot

Select favourite mirrors for AUR and PACMAN

>nano /etc/pacman.d/mirrorlist

Prepare base system packages

>pacstrap /mnt base base-devel amd-ucode htop neofetch git feh scrot openssh grub zsh efibootmgr sudo linux linux-firmware networkmanager dhcpcd mkinitcpio

Generate mounting informations

>genfstab-U /mnt >> /mnt/etc/fstab

Check all is right

>cat /mnt/etc/fstab

Go deep into the new disk install

>arch-chroot /mnt

Enable DHCP for your system

>systemctl enable dhcpcd

Set timezone to Europe/Brussels

>ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime

Set hardware clock from system

>hwclock --systohc

Select which locale to use

>nano /etc/locale.gen

uncomment `en_US.UTF-8`, save

Generate new language config

>locale-gen

>echo "LANG=en_US.UTF-8" >> /etc/locale.conf
>echo "KEYMAP=be-latin1" >> /etc/vconsole.conf

Set hostname as wanted

>MY_HOSTNAME=host

>echo $MY_HOSTNAME > /etc/hostname

>echo "127.0.0.1 localhost" >> /etc/hosts

>echo "::1 localhost" >> /etc/hosts

>echo "127.0.1.1 $MY_HOSTNAME.localdomain $MY_HOSTNAME" >> /etc/hosts

Create boot manager record (EFI only)

>efibootmgr --disk /dev/xxx

Create grub folder

>mkdir /boot/grub

Generate the config for the cpio

>mkinitcpio -p linux

Install grub on the EFI

>grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB

Generate the config and store it on the boot folder

>grub-mkconfig -o /boot/grub/grub.cfg

set root password

>passwd

add user

>useradd lumi

leave the chroot

>exit

Unmount disks

>umount /mnt/boot
>umount /mnt

reboot system

>reboot

log back in

System ready!
