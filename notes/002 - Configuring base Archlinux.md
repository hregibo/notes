---
tags: [arch, Linux]
title: 002 - Configuring base Archlinux
created: '2019-03-15T20:39:18.584Z'
modified: '2019-12-13T13:51:42.308Z'
---

# 002 - Configuring base Archlinux

### copy timezone data
```bash
ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime
```
### sync hardware clock
```bash
hwclock --systohc
```
### uncomment `en_US.UTF-8` locale & generate locales
```bash
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
locale-gen # generates locales
```
### insert locale on conf file
```bash
echo "LANG=en_US.UTF-8" > /etc/locale.conf
```
### setting keyboard mapping
```bash
echo "KEYMAP=be-latin1" > /etc/vconsole.conf
```
### setting hostname
```bash
echo "suomi" > /etc/hostname
```
### setting hosts file
```bash
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "127.0.1.1 suomi.localdomain suomi" >> /etc/hosts
```
### generate grub loader
```bash
grub-install --target=i386-pc /dev/sdb
```
### apply microcode early updates
```bash
grub-mkconfig -o /boot/grub/grub.cfg
```
### set password for root user
```bash
passwd
```
### regenerate mkinitcpio
```bash
mkinitcpio -p linux
```
### set systemctl features we want always on
```bash
systemctl enable dhcpcd
```
### exit chroot
```bash
exit
```
