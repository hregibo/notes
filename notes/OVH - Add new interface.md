---
tags: [DevOps]
title: OVH - Add new interface
created: '2020-10-04T21:22:18.564Z'
modified: '2020-11-01T13:30:59.427Z'
---

# OVH - Add new interface

Link Interface in OVH Public Cloud UI, then do this:

```bash
ip a add $IPADDR dev eth0
```

this is a temporary addition. To add permanently, edit the following:
```
# /etc/network/interfaces

auto eth0:0 # (or :1, :2, :3, ...)
iface eth0:0 inet static
  address xxx.xxx.xxx.xxx
  netmask 255.255.255.255
  broadcast xxx.xxx.xxx.xxx
```
thats it :)
