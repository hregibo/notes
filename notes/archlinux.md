# Installing Archlinux

I am considering you already set up the disks, made the filesystems and mounted them in `/mnt`.

```bash
# Base packages
pacstrap /mnt --needed base base-devel amd-ucode linux linux-firmware linux-headers dhcpcd mkinitcpio mkinitcpio-nfs-utils dosfstools tzdata unicode-character-database

# Generic utilities
pacstrap /mnt --needed htop git scrot openssh zsh zip xz neovim unzip unrar tar smbclient rsync mdadm cryptsetup pacman-contrib pacman-mirrorlist nfs-utils kbd licenses gnupg ffmpeg ca-certificates ca-certificates-mozilla ca-certificates-utils wget curl 

# Fonts
pacstrap /mnt --needed ttf-cascadia-code woff2 adobe-source-code-pro-fonts

# GUI-related
pacstrap /mnt --needed xorg-xinit xorg-server i3status i3lock i3-gaps xorg-xrandr rofi xorg-font-util xorg-setxkbmap pavucontrol breeze-icons dmenu dunst dbeaver vlc wireplumber x264 x265

# SysAdmin and DevOps tools
pacstrap /mnt --needed python-pip terraform consul packer vault rustup postgresql-libs nmap mtr ipcalc ipv6calc iana-etc inetutils net-tools iputils iso-codes go docker docker-compose docker-machine docker-scan 

# install the /etc/fstab file
genfstab -U /mnt >> /etc/fstab

# enter chroot mode
arch-chroot /mnt
```

Now that we are in CHROOT, we can run some commands internally:
```bash
# start dhcpcd on computer boot
systemctl enable dhcpcd

# set the timezone
ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime

# remove comment on EN
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen

# generate locales
locale-gen

# set computer locale
echo "LANG=en_US.UTF-8" > /etc/locale.conf

# set hostname
PC_HOSTNAME="helsinki"
echo "$PC_HOSTNAME" > /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $PC_HOSTNAME.localdomain $PC_HOSTNAME" >> /etc/hosts

# install boot manager
bootctl install

# generate mkinitcpio
mkinitcpio -p linux

# create user
useradd hugo
# add user to docker group and sudo users
usermod -aG docker sudo wheel hugo

# change root password
passwd
```

As the new user:

```bash
# set which wm to use with `startx`
echo "exec i3" >> ~/.xinitrc

# copy default i3 config
cp /etc/i3/config ~/.config/i3/config

# set some more parameters for i3
mkdir -p ~/.config/i3
echo "for_window [class=\".*\"] border pixel 0" >> ~/.config/i3/config
sed -i 's/bindsym Mod1+d exec --no-startup-id dmenu_run/#bindsym Mod1+d exec --no-startup-id dmenu_run/g' ~/.config/i3/config
sed -i 's/# bindsym Mod1+d exec "rofi -modi drun,run -show drun"/bindsym Mod1+d exec "rofi -modi drun,run -show drun"/g' ~/.config/i3/config
```

Good to go, we can reboot now.

