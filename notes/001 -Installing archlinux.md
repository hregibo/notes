---
tags: [arch, Linux]
title: 001 -Installing archlinux
created: '2019-03-15T20:39:12.255Z'
modified: '2019-12-13T13:51:37.494Z'
---

# 001 -Installing archlinux

### Setting up keyboard layout
```bash
loadkeys be-latin1
```
### setting NTP servers for date
```bash
timedatectl set-ntp true
```
### partition stuff
```bash
fdisk -l # list disks & partitions
cfdisk /dev/sdX # handle partitions
```
> do a main partition (LINUX FILESYSTEM), a SWAP partition (Linux SWAP), a +1M boot partition (BOOT Bios)

### create filesystems
```bash
mkfs.ext4 /dev/sdb1
mkswap /dev/sdb2
swapon /dev/sdb2
```
### mount disk to /mnt
```bash
mount /dev/sdb1 /mnt
```
### install base required things
```bash
pacstrap /mnt base base-devel git grub intel-ucode neofetch sudo htop openssh
```
### generate fstab
```bash
genfstab -U /mnt >> /mnt/etc/fstab
```
### chroot into install
```bash
arch-chroot /mnt
```
