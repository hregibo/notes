---
tags: [Linux]
title: Wireguard
created: '2020-07-31T01:09:36.569Z'
modified: '2020-10-30T08:19:32.913Z'
---

# Wireguard

## Package install

On some systems (well, all of those i used) the install failed silently as headers weren't available

To fix that, use the following:
```bash
# debian
apt-get install linux-headers-$(uname -r)
# arch (should not happen but eh)
pacman -S linux-headers-$(uname -r)
```

then, install tools
```bash
# debian
apt-get install --reinstall wireguard-dkms wireguard-tools
# arch
pacman -S wireguard-dkms wireguard-tools
```

once this is done, enable on `Server` the forwarding of traffic from ipv4 and ipv6, uncomment the following lines:

```bash
# /etc/sysctl.conf
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1
```

Then apply changes

```bash
sysctl -p
```

---

## Configure network for IPv6 (optional, SERVER ONLY)

Head to your dashboard of public cloud, copy the IPV6 & IPV6_GATEWAY somewhere. you'll just have to paste it without any changes into the file below.

Then, connect to the `Server` using SSH, and edit the `/etc/network/interfaces` file:

```bash
# add the following to the internet connection, ie. eth0
iface eth0 inet6 static
address *****IPV6*****
netmask 128
post-up /sbin/ip -6 route add *****IPV6_GATEWAY***** dev eth0
post-up /sbin/ip -6 route add default via *****IPV6_GATEWAY***** dev eth0
pre-down /sbin/ip -6 route del default via *****IPV6_GATEWAY***** dev eth0
pre-down /sbin/ip -6 route del *****IPV6_GATEWAY***** dev eth0
```

Once that's done, our IPv6 is ready to be used. We can set a `AAAA` DNS record for instance with `IPV6` as a destination. But it's out of scope.

Restart Networking for the IPv6 to be effective:

```bash
systemctl restart Networking
```

---

## Configuring Wireguard

Wireguard has a tool called `wg-quick` that will allow us to up/down easily the network of the VPN.

Generate a private key locally for wireguard, and generate the public key too:

```bash
umask 077
wg genkey > ~/privatekey
wg pubkey < ~/privatekey > ~/publickey
```

**DO THIS FOR BOTH SERVER AND CLIENT !**

Save the following file in the CLIENT side, on `/etc/wireguard/wg0.conf`:

```ini
[Interface]
PrivateKey = YOUR_GENERATED_CLIENT_PRIVATE_KEY
ListenPort = 51820
# The IPv6 part is optional if your server does not have it enabled
Address = 10.0.0.10/32, fc99::10/128

# # THIS IS A KILLSWITCH FOR UNENCRYPTED DATA THAT WILL DROP
#FwMark = 31625
#Table = 31625
#PostUp = iptables -I OUTPUT ! -o %i -m mark ! --mark 31625 -m addrtype ! --dst-type LOCAL -j REJECT
#PreDown = iptables -D OUTPUT ! -o %i -m mark ! --mark 31625 -m addrtype ! --dst-type LOCAL -j REJECT

# # THIS IS TO ALLOW THE OTHER PEERS TO ACCESS LOCAL NETWORK
# # Be sure to replace enp6s0 with your actual network adapter
PostUp   = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o enp6s0 -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o enp6s0 -j MASQUERADE

[Peer]
PublicKey = YOUR_GENERATED_SERVER_PUBLIC_KEY
# Setting the AllowedIPs to 0.0.0.0 means that locally, any traffic of any network card will be forwarded towards the server, hence providing a truely encrypted usage of your network
# The IPv6 is optional if your server does not have IPv6 enabled
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = SERVER_PUBLIC_IP:55460
PersistentKeepalive = 25
```

Save the following file in the SERVER side, on `/etc/wireguad/wg0.conf`:

```ini
[Interface]
# if this server does not work with IPv6, remove that part
Address = 10.0.0.1/32, fc99:0000:0000::1/128
# be sure that this port is the same as in the "Endpoint" of the client config
ListenPort = 55460
PrivateKey = YOUR_GENERATED_SERVER_PRIVATE_KEY

[Peer]
PublicKey = YOUR_GENERATED_CLIENT_PUBLIC_KEY
# by adding the remote client's LAN range, we forward our queries in this computer to the peer having the mapping
# IF the server does not support IPv6, remove that IPv6 part
AllowedIPs = 10.0.0.10/32, 192.168.0.0/24, fc99:0000:0000::10/128
```

---

## Run wireguard

On both computers, run `wg-quick up wg0` and let the magic happen :)

To verify on client side that everything forwards to the server, you can do the following:

```bash
curl zx2c4.com/ip
```

it should output your server IP instead of your client IP

To verify that the IPv6 of your server is well configured, on the server itself, do this:

```bash
ping6 ipv6.google.com
```

if the ping goes trough, perfect!

for client ipv6 verification of the server, do this: 

```bash
ping <AAAA_RECORD>.<YOUR_DOMAIN> 
# might require you to use ping6 instead, depends
```

if packets go through, fantastic.

To verify that your IPV6 packets go through your server and then to the actual ipv6 server, same as for the server, ping `ipv6.google.com` :)

that's it!


## Generate Config for mobiles (QRCode)

First of all, the server needs to have the `qrencode` package

```bash
# debian
apt-get install qrencode
# arch
pacman -S qrencode
```

Generate the private and public key just like before, save it with another name than you used

create a file based on "client" one we had before

add the peer to the server part of the config

use the following command to create a QRCode for mobile to scan:

```bash
qrencode -t ansiutf8 < your-wireguard-config.conf
```

it will output a QRCode in console. Scan it with phone, enable the VPN, gg ez!
