---
favorited: true
tags: [Programming]
title: Project ideas
created: '2019-01-15T12:59:28.146Z'
modified: '2020-10-30T12:36:23.934Z'
---

# Project ideas

* Co-working office on the fly

* Twitch Break bot/website
  * configurable duration of breaks and delay between breaks
  * configurable time to be added if previous break is ignored and/or skipped
  * have a bot connect to the dude's channel
  * verify each minute each channel, for how long they are live, and if it matches the delay since last break
  * should listen to "go live" events from PubSub of twitch apis
  * message should be configurable eventually, not a priority
  * allow them to have a custom page displaying the countdown of the break
    * ie if its a 5min break, a countdown of 5mins, formatted per channel as they want
    * no background or anything so they can customize it on their stream overlay themselves
    * pure text

* api monitoring
  * stats response time, error calling it, ...

* todo app (eh..)

* SSO auth service
  * just login and show the success
  * user management
  * account management
  * valid domains
  * support SAML (?)
  * support LDAP (?)
  * support OAuth (?)

* multi-room chat website
  * one room, no auth
  * one room, auth
  * unlimited rooms, auth
  * persistence
  * moderation rights
  * extra features (motd, ban/kick, ...)

* touché coulé with websockets

* issue tracker
  * bugs
  * tasks
  * todos
  * ...etc

* uri shortener
  * base (ui, this domain)
  * api call for uri
  * registration
  * domain registration, use own domain for redirect


